# Dry

## Q1
2. It is possible for programmer to overload operators, here is one example of overloading negation operator for struct S:
   ```d
   struct S
   {
       int m;

       int opUnary(string s)() if (s == "-")
       {
           return -m;
       }
   }

   int foo(S s)
   {
       return -s;
   }
   ```
3. It is possbile to specify multiple definitions for a function (procedure) name in the same scope by defining functions implementations which take different arguments (for example, arguments of different types), here is an example for that:
   ```d
   class printData { 
      public: 
         void print(int i) { 
            writeln("Printing int: ",i); 
         }

         void print(double f) { 
            writeln("Printing float: ",f );
         }

         void print(string s) { 
            writeln("Printing string: ",s); 
         } 
   }; 
   ```

## Q2
1. It's untyped (in other words, dynamically typed) language since there are no type declarations an therefore typed will be checked during runtime.
2. It's weakly typed language since there is reassignment of value to a variable `x` (first value was `3` (integer) and the second value is `"negative input"` (string)).
3. Since untyped is basically means the same as dynamically typed, language above is dynamically typed.
4. There is an implicit typing since no types are specified during assignments or variables declarations.