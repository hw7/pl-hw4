use "dictionary.sml";

(* insert *)
val d = Nil;
val d = insert d 2 #"a";
val d = insert d 2 #"b";
val d = insert d 1 #"a";
val d = insert d 3 #"c";
val d = insert d 1 #"a";
val d = insert d 1 #"g";
val d = insert d 3 #"m";

(* find *)
find d 2;
(* find d 4; *)

(* remove *)
insert Nil "a" [1,2];
remove it "a";
(* remove it "a"; *)

(* keys *)
keys d;

(* values *)
values d; 

(* map_dict *)
map_dict (fn (a,b) => chr (a + (ord b))) d;

(* sorted_items *)
sorted_items op< d;