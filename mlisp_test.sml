use "mlisp.sml";

(* parse *)
ATOM(SYMBOL("hello")) = (parse (tokenize "hello"));

tokenize "(+ 2 3)";

CONS(
   ATOM(SYMBOL("+")),
   CONS(
       ATOM(NUMBER(2)),
       CONS(
           ATOM(NUMBER(3)),ATOM(NIL)
       )
   )
) = (parse (tokenize "(+ 2 3)"));

CONS(
    ATOM(NUMBER(3)),
    CONS(
        CONS(
            ATOM(NUMBER(1)),
            CONS(
                ATOM(NUMBER(2)),ATOM(NIL)
            )
        ),ATOM(NIL)
    )
) = (parse (tokenize "(3 (1 2))"));

CONS(
   ATOM(SYMBOL("+")),
   CONS(
       ATOM(NUMBER(1)),
       CONS(
           CONS(
               ATOM(SYMBOL("+")),
               CONS(
                   ATOM(NUMBER(1)),
                   CONS(
                       ATOM(NUMBER(3)),ATOM(NIL)
                   )
               )
           ),ATOM(NIL)
       )
   )
) = (parse (tokenize "(+ 1 (+ 1 3))"));

CONS(
   ATOM(SYMBOL("+")),
CONS(
       CONS(
           ATOM(SYMBOL("+")),
           CONS(
               ATOM(NUMBER(1)),
               CONS(
                   ATOM(NUMBER(3)),ATOM(NIL)
               )
           )
       )
       ,CONS(
           ATOM(NUMBER(1)),ATOM(NIL)
       )
   )
) = (parse (tokenize "(+ (+ 1 3) 1)"));

CONS(
   ATOM(SYMBOL("define")),
   CONS(
       ATOM(SYMBOL("r"))
       ,CONS(
           ATOM(NUMBER(5)),ATOM(NIL)
       )
   )
) = (parse (tokenize "(define r 5)"));

