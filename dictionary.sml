datatype ('a, 'b) dictionary = 
    Nil
  | Dict of {key: 'a, value: 'b} list;
exception ItemNotPresent;

local
  fun exists Nil _ = false
    | exists (Dict []) _ = false
    | exists (Dict ({key = elemKey, value = _} :: xs)) k =
      if elemKey = k then true else exists (Dict xs) k

  fun append Nil x = Dict (x :: [])
    | append (Dict d) x = Dict (x :: d)

  fun removeAux Nil _ _ = Nil
    | removeAux (Dict []) _ _ = Dict []
    | removeAux (Dict d) k n =
      let
        val left = List.take (d, n)
        val rightWithElem = List.drop (d, n)
        val elemKey = #key (List.hd rightWithElem)
      in
        if elemKey = k then Dict (left @ (List.tl rightWithElem))
        else removeAux (Dict d) k (n + 1)
      end 

  fun getSize Nil = 0
    | getSize (Dict d) = List.length d

  fun findAux Nil _ = raise ItemNotPresent
    | findAux (Dict []) _ = raise ItemNotPresent
    | findAux (Dict ({key = elemKey, value = elemVal} :: xs)) k =
      if elemKey = k then elemVal else findAux (Dict xs) k

  fun transMapDict f = fn {key = k, value = v} => {key = k, value = f (k, v)}

  fun transSortedItems f = 
    fn ({key = k1, value = v1}, {key = k2, value = v2}) => f (k1, k2)

  fun sort cmp list = foldr (fn (x, lst) => List.filter (fn a => cmp (a, x)) lst @ [x] @ List.filter (fn a => cmp(a, x) <> true) lst) [] list
in
  fun remove d k =
    if (exists d k) = false then raise ItemNotPresent
    else if (getSize (removeAux d k 0)) = 0 then Nil else (removeAux d k 0)
  
  fun insert d k v =
    if exists d k then append (remove d k) {key = k, value = v}
    else append d {key = k, value = v}

  fun find d k =
    if (exists d k) = false then raise ItemNotPresent
    else findAux d k

  fun keys Nil = []
    | keys (Dict []) = []
    | keys (Dict ({key = k, value = _} :: xs)) = k :: (keys (Dict xs))

  fun values Nil = []
    | values (Dict []) = []
    | values (Dict ({key = _, value = v} :: xs)) = v :: (values (Dict xs))

  fun map_dict f Nil = Nil
    | map_dict f (Dict []) = Dict []
    | map_dict f (Dict d) = Dict (List.map (transMapDict f) d)
  
  fun sorted_items cmp Nil = Nil
    | sorted_items cmp (Dict []) = Dict []
    | sorted_items cmp (Dict d) = Dict (sort (transSortedItems cmp) d)
end