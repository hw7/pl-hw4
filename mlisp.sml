datatype Atom =
  SYMBOL of string | NUMBER of int | NIL;

datatype SExp =
  ATOM of Atom | CONS of (SExp * SExp);

local
  fun concatenateList [] currentString = currentString
    | concatenateList (x :: xs) currentString = 
        (concatenateList xs (currentString ^ x)) 

  local
    fun numOfOpenersAux (s, i) =
    if i < 0 then 0 else if String.sub (s, i) = #"(" then 1 + numOfOpenersAux (s, i - 1) else numOfOpenersAux (s, i - 1);

    fun numOfOpeners s =
      numOfOpenersAux (s, size s - 1);

    fun numOfClosersAux (s, i) =
      if i < 0 then 0 else if String.sub (s, i) = #")" then 1 + numOfClosersAux (s, i - 1) else numOfClosersAux (s, i - 1);

    fun numOfClosers s =
      numOfClosersAux (s, size s - 1);

    fun balanceAux (s, i) =
      if i < 0 then true else if numOfOpenersAux (s, i) >= numOfClosersAux (s, i) then balanceAux (s, i - 1) else false;

    fun balance s =
      if numOfOpeners s = numOfClosers s then balanceAux (s, size s - 1) else false;
  in
    fun balanced tokens = balance (concatenateList tokens "")
  end
  
  fun getNextList [] currentTokens = []
    | getNextList (x :: xs) currentTokens =
        if balanced currentTokens then currentTokens
        else getNextList xs (currentTokens @ [x])
  
  fun getNextElement [] = []
    | getNextElement ("(" :: xs) =
        getNextList xs ["("]
    | getNextElement (x :: xs) = [x]

  fun removeNextList [] currentTokens = []
    | removeNextList (x :: xs) currentTokens =
        if balanced currentTokens then (x :: xs)
        else removeNextList xs (currentTokens @ [x])      

  fun removeNextElement [] = []
    | removeNextElement ("(" :: xs) =
        removeNextList xs ["("]
    | removeNextElement (x :: xs) = xs

  fun atomSexp s =
    if isNumber s then ATOM (NUMBER (atoi s))
    else ATOM (SYMBOL s)
  
  fun parseList [] = ATOM NIL
    | parseList (")" :: _) = ATOM NIL 
    | parseList tokens =
        let
          val nextElement = getNextElement tokens
          val nextElementIsList = (List.length nextElement) > 1
          val tokensLeft = removeNextElement tokens
        in
          if nextElementIsList then CONS (parseList (List.tl nextElement), parseList tokensLeft)
          else CONS (atomSexp (List.hd nextElement), parseList tokensLeft)
        end
in
  fun parse [] = ATOM NIL
    | parse (x :: xs) =
      if x <> "(" then atomSexp x
      else 
        if ((List.length xs) = 1) andalso ((List.hd xs) = ")") then ATOM NIL
        else parseList xs
end;
